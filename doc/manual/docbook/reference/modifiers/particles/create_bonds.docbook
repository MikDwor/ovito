<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="particles.modifiers.create_bonds"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Create bonds</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/modifiers/create_bonds_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>

	  This modifier creates new <link linkend="scene_objects.bonds">bonds</link> between pairs of particles according to a distance-based criterion.
    Existing bonds will not be affected by the modifier, and it will not create another bond for a pair of particles that is already connected by a bond.

    <informaltable frame="none" colsep="0" rowsep="0">
      <tgroup cols="2">
        <tbody>
          <row valign="bottom">
            <entry>Input:</entry>
            <entry>Output:</entry>
          </row>
          <row valign="top">
            <entry>
          <mediaobject><imageobject>
          <imagedata fileref="images/modifiers/create_bonds_example_input.png" format="PNG" />
          </imageobject></mediaobject>
            </entry>
            <entry>
          <mediaobject><imageobject>
          <imagedata fileref="images/modifiers/create_bonds_example_output.png" format="PNG" />
          </imageobject></mediaobject>
            </entry>
          </row>
        </tbody>
      </tgroup>
    </informaltable>

  </para>

  <para>
    The modifier supports three modes of operation that use different criteria to decide which particles to connect by a bond:
    <variablelist>
      <varlistentry>
        <term>Uniform cutoff distance</term>
        <listitem>
          <para>The same uniform cutoff distance is used to create bonds between pairs of particles, irrespective of their respective type(s).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Van der Waals radii</term>
        <listitem>
          <para>A bond is created between two atoms if their separation is less than 60% of the sum of their van der Waals radii. This standard criterion has been 
                adopted from the popular software <emphasis>VMD</emphasis>. The Van der Waals radii of all particle types of the system are displayed in the table. OVITO initializes these 
                values during file import based on the chemical element names found in the input file. If needed, you can override the standard Van der Waals radius of each atom type 
                in the <link linkend="scene_objects.particle_types">particle type</link> editor or, permanently, in the <link linkend="application_settings.particles">application settings</link> dialog. 
                The <emphasis>Create bonds</emphasis> modifier will only create bonds between pairs of particles which both have a positive Van der Waals radius.
          </para>
          <para>
            Furthermore, the <emphasis>Don't generate H-H bonds</emphasis> option turned on by default, which means the modifier will not generate any bonds connecting 
            two hydrogen atoms, i.e., which both have a particle type named "H" - even if they fulfill the distance-based criterion.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Pair-wise cutoffs</term>
        <listitem>
          <para>
            This mode gives you full control over the bond distance cutoff for each pair-wise combination of particle types.
            The table lists all pair-wise type combinations defined for the current system, and some of the cutoff avlues in the third column may already be pre-initialized according to the Van der Waals 
            criterion described above. A positive cutoff value is needed to create bonds between pairs of particles of the given type(s).
            Note that this mode is only available if particle types have been defined for the system, i.e., the particle property <literal>Particle Type</literal> exists.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </para>

  <para>
    The <emphasis>Suppress inter-molecular bonds</emphasis> option restricts generation of bonds to particles that
    are part of the same molecule, i.e. which have matching values of the <literal>Molecule Identifier</literal> property.
    If the <literal>Molecule Identifier</literal> particle property is not defined for the system, this option has no effect.
  </para>

  <para>
    The modifier lets you specify an optional <emphasis>lower cutoff</emphasis> value. It effectively restricts the generation of bonds
    to the distance interval between the lower and upper cutoffs.
  </para>

  <para>
    The modifier defines a new bond type, which is assigned to the newly created bonds.
    The properties of this bond type, in particular its name and display color, can be edited in the second parameter panel.
    Furthermore, the modifier will automatically create a <link linkend="visual_elements.bonds">bonds</link> visual element if needed, 
    which lets you control the visual appearance of bonds. The display settings of this visual element are found in the third parameter panel.
  </para>

  <para>
    As for particles, OVITO supports the assignment of arbitrary properties to bonds which have been generated by this modifier or which were created during simulation file import.
    Certain bond properties (see <link linkend="usage.bond_properties">this section</link>) are used by OVITO
    to control the visualization of bonds. By changing the values of these properties, for example using the <link linkend="particles.modifiers.compute_property">Compute property</link> modifier,
    you can adjust the visual representation of individual bonds.
  </para>

  <simplesect>
    <title>Technical notes</title>
 	  <para>
      OVITO stores a triplet of integer numbers with every bond in the <literal>Periodic Image</literal> property field.
      This triplet specifies whether a bond crosses the periodic boundaries of the simulation cell (if any) and in which direction.
      For example, a bond crossing the periodic cell boundary in the positive X direction is associated with the triplet (1,0,0) and
      will be visualized as two separate half bonds, one on either end of the cell. Bonds in the interior of the simulation box which do not cross a
      periodic boundary have a <literal>Periodic Image</literal> value of (0,0,0).
 	  </para>
  </simplesect>

  <simplesect>
    <title>See also</title>
    <para>
      <pydoc-link href="modules/ovito_modifiers" anchor="ovito.modifiers.CreateBondsModifier"><classname>CreateBondsModifier</classname> (Python API)</pydoc-link>
    </para>
  </simplesect>

</section>